import QtQuick

Text {
  color: "#ebdbb2"
  font.family: "Fira Sans Nerd Font"
  font.weight: Font.Black
  font.pixelSize: 16
  text: Time.time
  anchors.verticalCenter: parent.verticalCenter
}
